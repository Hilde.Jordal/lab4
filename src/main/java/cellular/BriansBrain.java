package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

	IGrid currentBrain; 
	
	public BriansBrain(int rows, int columns) {
		currentBrain = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}
	
	@Override
	public CellState getCellState(int row, int column) {
		// TODO Auto-generated method stub
		return currentBrain.get(row, column);
	}

	@Override
	public void initializeCells() {
		// TODO Auto-generated method stub
		Random random = new Random();
		for (int row = 0; row < currentBrain.numRows(); row++) {
			for (int col = 0; col < currentBrain.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentBrain.set(row, col, CellState.ALIVE);
				} else {
					currentBrain.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public void step() {
		// TODO Auto-generated method stub
		IGrid nextGeneration = currentBrain.copy();
    	for (int i = 0; i < currentBrain.numRows(); i++) {
    		for (int j = 0; j < currentBrain.numColumns(); j++) {
    			nextGeneration.set(i, j, getNextCell(i, j));
    		}
    	}
    	currentBrain = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		// TODO Auto-generated method stub
		CellState newState = null;
		int aliveNeighbors = this.countNeighbors(row, col, CellState.ALIVE);
		if (getCellState(row, col).equals(CellState.ALIVE)) {
				newState = CellState.DYING;
		} else if (getCellState(row, col).equals(CellState.DYING)) {
				newState = CellState.DEAD;
		} else if (getCellState(row, col).equals(CellState.DEAD)) {
				if (aliveNeighbors == 2) {
					newState = CellState.ALIVE;
				} else {
					newState = CellState.DEAD;
				}
		} else {
			newState = newState;
		}
		return newState;
	}
	
	private int countNeighbors(int row, int col, CellState state) {
		// TODO
		int stateCount = 0;
		for (int i = row-1; i <= row+1; i++) {
			for (int j = col-1; j <= col+1; j++) {
				try {
					if (!((i == row) && (j == col))) {
						if (currentBrain.get(i, j).equals(state)) {
							stateCount ++;
							}
						}
					}
				catch(IndexOutOfBoundsException e) {
					continue;
					}
			}
		}
		return stateCount;
	}

	@Override
	public int numberOfRows() {
		// TODO Auto-generated method stub
		return currentBrain.numRows();
	}

	@Override
	public int numberOfColumns() {
		// TODO Auto-generated method stub
		return currentBrain.numColumns();
	}

	@Override
	public IGrid getGrid() {
		// TODO Auto-generated method stub
		return currentBrain;
	}

}
