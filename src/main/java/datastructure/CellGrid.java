package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
	
	int rows;
	int cols;
	CellState currentCellState[][];
	

    public CellGrid(int rows, int columns, CellState initialState) {
    	this.rows = rows;
    	this.cols = columns;
    	currentCellState = new CellState[rows][cols];
    	for (int i = 0; i < rows; i++) {
    		for (int j = 0; j < cols; j++) {
    			currentCellState[i][j] = initialState;
    		}
    	}
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (!((row >= 0) || (row < numRows()) || (column >= 0) || (column < numColumns()))) {
        	throw new IndexOutOfBoundsException();
        }
        currentCellState[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) throws IndexOutOfBoundsException {
        // TODO Auto-generated method stub
        if ((row < 0) || (row >= numRows()) || (column < 0) || (column >= numColumns())) {
        	throw new IndexOutOfBoundsException();
        }
        return currentCellState[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
    	CellGrid copyCellGrid = new CellGrid(this.rows, this.cols, CellState.DEAD);
    	for (int i = 0; i < this.rows; i++) {
    		for (int j = 0; j < this.cols; j++) {
    			copyCellGrid.set(i, j, this.get(i, j));
    		}
    	}
        return copyCellGrid;
    }
    
}
